// lifeincodes-hoverjs v1.0.0
// Licensed under the terms of the MIT license.

// People involved
//  - Vlad Petrescu (maintainer)

// default values:
const options = {
	debugMode: false,
	waitTime: 750,
	selector: 'a',
	disableBrowserTitleHover: true
}

var wrapperDiv;
var scrollableDiv;
var arrowDiv;
var isResizing = false;
var isPositionedTop;
var issueKey;
var targetOffsetTop;
var targetOffsetLeft;
var wrapperOffsetTop;
var wrapperOffsetLeft;
var wrapperOffsetBottom;
var targetEl;

function LifeInCodesHover() {
	this.init = function (options_) {
		if (options_ != null) {
			if (options_.backendUrl == null) {
				console.error("A backendUrl parameter must be defined when using LifeInCodesHover.");
				return;
			}

			if (options_.$ != null) {
				$ = options_.$;
			}
			if (options_.source != null) {
				options.source = options_.source;
			}
			if (options_.requestHeaders != null) {
				options.requestHeaders = options_.requestHeaders;
			}
			if (options_.selector != null) {
				options.selector = options_.selector;
			}
			if (options_.debugMode != null) {
				options.debugMode = options_.debugMode;
			}
			if (options_.waitTime != null) {
				options.waitTime = options_.waitTime;
			}
			options.backendUrl = options_.backendUrl;
			if (options_.getIssueKeyFunc != null) {
				options.getIssueKeyFunc = options_.getIssueKeyFunc;
			}
			if (options_.disableBrowserTitleHover != null) {
				options.disableBrowserTitleHover = options_.disableBrowserTitleHover;
			}
		} else {
			console.error("No options have been passed to LifeInCodesHover. Nothing to do here.");
			return;
		}

		attachEvents($(document), null);

		// iterate all iframes and attach hovers (ex: in jira dashboard)
		iterateIframesAndAttachHover();

		// create an observer instance
		const observer = new MutationObserver(function (mutations) {
			mutations.forEach(function () {
				iterateIframesAndAttachHover();
			});
		});

		// configuration of the observer:
		const config = {childList: true, subtree: true}

		// pass in the target node, as well as the observer options
		observer.observe(document, config);
	};

	const iterateIframesAndAttachHover = function () {
		setInterval(function () {
			try {
				$(document).find("iframe").each(function (i, iframee) {
					if (iframee.src && !iframee.hoverAttached) {
						// attach events on visible elements
						attachEvents($(iframee).contents(), $(iframee));

						// create an observer on changes of iframe, and attach events when they happen
						const observer = new MutationObserver(function () {
							attachEvents($(iframee).contents(), $(iframee));
						});
						observer.observe(iframee, {attributes: true, childList: true});
						iframee.hoverAttached = true;
					}
				});
			} catch (e) {
				console.error(e);
			}
		}, 100);
	};

	const attachEvents = function (container, iframe) {
		container.on("mouseenter", options.selector, function (e) {
			e.preventDefault();
			if (options.debugMode) {
				console.log('mousemove ' + $(".issue-link:hover").length);
			}

			if (options.disableBrowserTitleHover) {
				if (e.target.title !== '') {
					e.target.link_title = e.target.title;
					e.target.title = '';
				} else {
					if (e.target.parentElement.nodeName === 'SPAN' && e.target.parentElement.title !== '') {
						e.target.parentElement.link_title = e.target.parentElement.title;
						e.target.parentElement.title = '';
					}
				}
			}

			//if the a has a img, it is a issue type image (happens in jira 8). do not add hover.
			//if the hover is resized over another elements, !targetEl.is("a") is needed (dont understand why)
			if ($(e.target).is(':empty') || !$(e.target).is("a") || isResizing) {
				return;
			}
			issueKey = options.getIssueKeyFunc == null ? getIssueKey($(e.target)) : options.getIssueKeyFunc($(e.target));

			if (issueKey == null && options.source !== 'bamboo') {
				return;
			}

			setTimeout(function () {
				setTimeout(function () {
					drawDiv(e, iframe);
				}, options.waitTime);
			}, 0);

		});

		if (!options.debugMode) {
			container.on("mouseout", options.selector, function (e, iframee) {
				setTimeout(function () {
					if (scrollableDiv === null || $("#issue-tracker:hover").length === 1) {
						return;
					}
					if (options.disableBrowserTitleHover) {
						if (e.target.link_title !== undefined && e.target.link_title !== '') {
							e.target.title = e.target.link_title;
							e.target.link_title = e.target.title;
						} else {
							if (e.target.parentElement.link_title !== '') {
								e.target.parentElement.title = e.target.parentElement.link_title;
								e.target.parentElement.link_title = e.target.parentElement.title;
							}
						}
					}

					if (!isResizing && e.target != null && e.target.id !== "issue-tracker" && (iframee == null ? !$(e.target).is(":hover") : $("iframe").contents().find("a:hover").length === 0)) {
						cleanUp(false);
					}
				}, options.waitTime);
			});
		}

		const getIssueKey = function (targetElTemp) {
			// for kanban boards, hovering is done on a inner span. need the ancestor a to get the issue key
			if (!targetElTemp.is("a")) {
				var found = false;
				while (targetElTemp.parent() != null && !found) {
					targetElTemp = targetElTemp.parent();
					if (targetElTemp.is("a")) {
						found = true;
					}
				}
				if (!found) {
					return null;
				}
			}

			var issueKeyReg = /\/browse\/(.+-\d+)[^\d]*$/;
			var iKey = targetElTemp.attr("data-issue-key");
			if (iKey) {
				return iKey.trim();
			} else {
				//in jira agile or for older jira versions.
				const href = targetElTemp.attr("href");
				const match = issueKeyReg.exec(href);
				if (match) {
					iKey = match[1];
					return iKey.trim();
				} else {
					return null;
				}
			}
		}

		const drawDiv = function (e, iframee) {
			// if the target is not hovered anymore, clean all and don't do anything
			if (!$(e.target).is(":hover")) {
				if (wrapperDiv == null || !wrapperDiv.is(":hover")) {
					if (targetEl != null && targetEl[0] === $(e.target)[0]) {
						cleanUp(true);
					}
				}
				return;
			} else {
				if (targetEl != null && targetEl[0] === $(e.target)[0]) {
					return;
				} else {
					targetEl = $(e.target);
				}
			}

			var urlParams = 'source=' + options.source;
			if (options.source === 'jira') {
			} else if (options.source === 'confluence') {
				urlParams += '&issueURL=' + targetEl.attr("href");
			} else if (options.source === 'bamboo') {
				// in bamboo there are 3 main tabs where we have issue links: Summary, Commits, Issues
				if (issueKey == null) {
					issueKey = targetEl.text();
					// Summary: the target is a direct url to the jira issue key, so get the issue url. also, the issue key is the text of the target
					urlParams += '&issueURL=' + targetEl.attr('href');
				} else {
					// Issues and Commits: parse the page url to get the bamboo project key
					const x = window.location.href.substr(window.location.href.lastIndexOf('/browse') + '/browse'.length + 1, window.location.href.length);
					urlParams += '&bambooProjectKey=' + x.substr(0, x.indexOf('-'));
				}
			} else {
				urlParams += '&issueKey=' + issueKey;
			}
			var fullUrl;
			if (options.source === 'jira' || options.source === 'confluence' || options.source === 'bamboo') {
				fullUrl = options.backendUrl + "/" + issueKey + "?" + urlParams;
			} else {
				if (options.backendUrl.includes("?")) {
					fullUrl = options.backendUrl + "&" + urlParams;
				} else {
					fullUrl = options.backendUrl + "?" + urlParams;
				}
			}

			$.ajax({
				url: fullUrl,
				dataType: 'json',
				beforeSend: function (xhr) {
					if (options.requestHeaders != null) {
						for (var key in options.requestHeaders) {
							xhr.setRequestHeader(key, options.requestHeaders[key]);
						}
					}
				},
				success: function (data) {
					if (data == null || data.value == null || data.value.indexOf('<div class="item"') !== 0) {
						cleanUp(true);
						return;
					}
					initVariables(iframee);

					if (wrapperDiv != null) {
						wrapperDiv.remove();
					}
					if (scrollableDiv != null) {
						scrollableDiv.remove();
					}

					wrapperDiv = $("<div id='issue-tracker' class='aui-inline-dialog dialog-contents hoverpopup' style='display: none'>");
					$('body').append(wrapperDiv);

					if (!options.debugMode) {
						wrapperDiv.on('mouseout', function () {
							setTimeout(function () {
								if (!isResizing && $("#issue-tracker:hover").length === 0 && !$(e.target).is(":hover") && scrollableDiv != null) {
									cleanUp(false);
								}
							}, options.waitTime, targetEl);
						});
					}

					scrollableDiv = $("<div id='scrollableDiv' class='property-list'>");
					wrapperDiv.append(scrollableDiv);

					showResizeArrows();
					scrollableDiv.append(data.value);
					positionWrapperDiv();
					drawArrows();
					setHeightOfWrapperDiv();

					// enable scroll. auto-hide after 5000ms
					scrollableDiv.niceScroll({
						horizrailenabled: false,
						cursorcolor: "#C1C1C1",
						hidecursordelay: 5000
					});

					// make it visible after it's populated
					wrapperDiv.css('display', 'inline-table');
				},
				error: function (data) {
					// if can not get data from the backend for any reason, clean up everything
					console.log("error getting details");
					cleanUp(true);
				}
			});
		}

		const initVariables = function (iframee) {
			targetOffsetTop = targetEl.offset().top;
			targetOffsetLeft = targetEl.offset().left;

			if (iframee != null) {
				//take in consideration the iframe's offset
				targetOffsetTop += iframee[0].getBoundingClientRect().top;
				targetOffsetLeft += iframee[0].getBoundingClientRect().left;
			} else {
				//take in consideration if the page is scrolled
				targetOffsetTop -= $(window).scrollTop();
				targetOffsetLeft -= $(window).scrollLeft();
			}

			// if the middle of the target is in the bottom of the screen, show the hover above the target
			isPositionedTop = targetOffsetTop + (targetEl.height() / 2) > $(window).height() / 2;
		};

		const showResizeArrows = function () {
			// when the hover is positioned below the target, show only the bottom resize icons
			if (isPositionedTop) {
				const resizerNW = $("<div id='resizerNW' class='resizer'>");
				const resizerNE = $("<div id='resizerNE' class='resizer'>");
				scrollableDiv.append(resizerNW);
				scrollableDiv.append(resizerNE);
				resizerNW.on('mousedown', ['nw'], initResize);
				resizerNE.on('mousedown', ['ne'], initResize);
			} else {
				// when the hover is positioned above the target, show only the top resize icons
				const resizerSE = $("<div id='resizerSE' class='resizer'>");
				const resizerSW = $("<div id='resizerSW' class='resizer'>");
				scrollableDiv.append(resizerSE);
				scrollableDiv.append(resizerSW);
				resizerSE.on('mousedown', ['se'], initResize);
				resizerSW.on('mousedown', ['sw'], initResize);
			}
		};

		const resize = function (e) {
			scrollableDiv.getNiceScroll().hide();
			if (e.data[0] === 'nw') {
				scrollableDiv[0].style.width = ($(window).width() - e.clientX - parseFloat(wrapperDiv.css('right')) - parseFloat(wrapperDiv.css('padding-left')) - parseFloat(wrapperDiv.css('padding-right'))) + 'px';
				scrollableDiv[0].style.height = ($(window).height() - e.clientY - wrapperOffsetBottom - parseFloat(wrapperDiv.css('padding-top')) - parseFloat(wrapperDiv.css('padding-bottom'))) + 'px';
			}
			if (e.data[0] === 'ne') {
				scrollableDiv[0].style.width = (e.clientX - wrapperOffsetLeft - parseFloat(wrapperDiv.css('padding-left')) - parseFloat(wrapperDiv.css('padding-right'))) + 'px';
				scrollableDiv[0].style.height = ($(window).height() - e.clientY - wrapperOffsetBottom - parseFloat(wrapperDiv.css('padding-top')) - parseFloat(wrapperDiv.css('padding-bottom'))) + 'px';
			}
			if (e.data[0] === 'se') {
				scrollableDiv[0].style.width = (e.clientX - wrapperOffsetLeft - parseFloat(wrapperDiv.css('padding-left')) - parseFloat(wrapperDiv.css('padding-right'))) + 'px';
				scrollableDiv[0].style.height = (e.clientY - wrapperOffsetTop - parseFloat(wrapperDiv.css('padding-top')) - parseFloat(wrapperDiv.css('padding-bottom'))) + 'px';
			}
			if (e.data[0] === 'sw') {
				scrollableDiv[0].style.width = ($(window).width() - e.clientX - parseFloat(wrapperDiv.css('right')) - parseFloat(wrapperDiv.css('padding-left')) - parseFloat(wrapperDiv.css('padding-right'))) + 'px';
				scrollableDiv[0].style.height = (e.clientY - wrapperOffsetTop - parseFloat(wrapperDiv.css('padding-top')) - parseFloat(wrapperDiv.css('padding-bottom'))) + 'px';
			}
		};

		const initResize = function (e) {
			// prevent default to disable the selection (after mouse is down, on mousemove the default behaviour is to select)
			e.preventDefault();

			// hide scroll while resizing. it does not render as fast as the resizing.
			scrollableDiv.getNiceScroll().hide();

			if (e.data[0] === 'nw') {
				// setting fixed the south and east
				wrapperDiv[0].style.top = "";
				wrapperDiv[0].style.right = $(window).width() - wrapperOffsetLeft - wrapperDiv.outerWidth() + "px";
				wrapperDiv[0].style.bottom = wrapperOffsetBottom + "px";
				wrapperDiv[0].style.left = "";
			}
			if (e.data[0] === 'ne') {
				// setting fixed the south and west
				wrapperDiv[0].style.top = "";
				wrapperDiv[0].style.right = "";
				wrapperDiv[0].style.bottom = wrapperOffsetBottom + "px";
				wrapperDiv[0].style.left = wrapperOffsetLeft + "px";
			}
			if (e.data[0] === 'se') {
				// setting fixed the north and west
				wrapperDiv[0].style.top = wrapperOffsetTop + "px";
				wrapperDiv[0].style.right = "";
				wrapperDiv[0].style.bottom = "";
				wrapperDiv[0].style.left = wrapperOffsetLeft + "px";

			}
			if (e.data[0] === 'sw') {
				// setting fixed the north and east
				wrapperDiv[0].style.top = wrapperOffsetTop + "px";
				wrapperDiv[0].style.right = $(window).width() - wrapperOffsetLeft - wrapperDiv.outerWidth() + "px";
				wrapperDiv[0].style.bottom = "";
				wrapperDiv[0].style.left = "";
			}

			if (e.data[0] === 'nw' || e.data[0] === 'sw') {
				// placing the arrow with a fixed right size to always remain just under/above the target element
				arrowDiv[0].style.right = wrapperDiv.width() - (arrowDiv.offset().left - wrapperOffsetLeft) + parseFloat(wrapperDiv.css('padding-left')) + parseFloat(wrapperDiv.css('padding-right')) + 'px';
				arrowDiv[0].style.left = "";

				// temporary changing so that the hover is always connected to the element you're hovering
				scrollableDiv[0].style['min-width'] = Math.max($(window).width() - parseFloat(wrapperDiv.css('right')) - arrowDiv.offset().left, 600) + 'px';
			} else {
				// placing the arrow with a fixed left size to always remain just under/above the target element
				arrowDiv[0].style.left = wrapperDiv.width() - parseFloat(arrowDiv[0].style.right) + parseFloat(wrapperDiv.css('padding-left')) + parseFloat(wrapperDiv.css('padding-right')) + 'px';
				arrowDiv[0].style.right = "";

				// temporary changing so that the hover is always connected to the element you're hovering
				scrollableDiv[0].style['min-width'] = Math.max(arrowDiv.offset().left - wrapperOffsetLeft, 600) + 'px';
			}

			$(window).on('mousemove', e.data, resize);
			$(window).on('mouseup', stopResize);

			// while hover is resizing, isResizing is true to makes sure the hover does not close while resizing
			isResizing = true;
		};

		const stopResize = function () {
			$(window).off('mousemove');
			$(window).off('mouseup');
			isResizing = false;
			// show the scroll
			scrollableDiv.getNiceScroll().show();
			// resize the scroll according to the new hover size
			scrollableDiv.getNiceScroll().resize();
			wrapperOffsetLeft = wrapperDiv.offset().left;
			wrapperOffsetTop = wrapperDiv.offset().top;
		};

		const positionWrapperDiv = function () {
			// set the top/bottom distance
			if (isPositionedTop) {
				wrapperOffsetBottom = $(window).height() - targetOffsetTop + 10;
				wrapperDiv[0].style.bottom = wrapperOffsetBottom + 'px';
			} else {
				wrapperOffsetTop = targetOffsetTop + targetEl.height() + 7;
				wrapperDiv[0].style.top = wrapperOffsetTop + 'px';
			}

			// set the right/left distance. ideally, it's 65% centered to the target (a bit to the right). if the target is too close to the margins of the screen,
			// put it at 10px distance to the margins
			if (targetOffsetLeft + wrapperDiv.width() > $(window).width()) {
				wrapperOffsetLeft = $(window).width() - wrapperDiv.width() - 25;
				// it's over the right margin
				wrapperDiv[0].style.right = '10px';

			} else {
				// it's over the left margin (or it's all visible)
				wrapperOffsetLeft = Math.max(targetOffsetLeft - wrapperDiv.width() * 0.35, 10);
				wrapperDiv[0].style.left = wrapperOffsetLeft + 'px';
			}
		};

		const drawArrows = function () {
			// adding the arrow
			if (isPositionedTop) {
				arrowDiv = $("<div class='aui-inline-dialog-arrow arrow aui-css-arrow aui-bottom-arrow'></div>");
			} else {
				arrowDiv = $("<div class='aui-inline-dialog-arrow arrow aui-css-arrow'></div>");
			}
			// calculated to be in the middle of the target
			arrowDiv[0].style.left = (targetOffsetLeft + targetEl.width() / 2 - wrapperOffsetLeft) + 'px';
			wrapperDiv.append(arrowDiv);
		};

		const setHeightOfWrapperDiv = function () {
			// if hover has a bigger height than the remainder of the screen, set it as the 70% of the remaining height
			// (it should have been 80%, but this takes in consideration #issue-tracker padding)
			if (isPositionedTop) {
				if (wrapperOffsetBottom + wrapperDiv.height() > $(window).height()) {
					scrollableDiv.height(Math.max(targetOffsetTop * 0.7, parseFloat(scrollableDiv.css('min-height'))) + 'px');
				}
			} else {
				if (wrapperOffsetTop + wrapperDiv.height() >= $(window).height()) {
					scrollableDiv.height(Math.max(($(window).height() - targetOffsetTop) * 0.7, parseFloat(scrollableDiv.css('min-height'))) + 'px');
				}
			}
		};

		const cleanUp = function (becauseOfError) {
			if (options.debugMode) {
				console.log('removing div');
			}
			if (!options.debugMode || becauseOfError) {
				$('#issue-tracker .nicescroll-rails').remove();
				if (wrapperDiv != null) {
					wrapperDiv.remove();
				}
				if (scrollableDiv != null) {
					scrollableDiv.remove();
					scrollableDiv = null;
				}
				arrowDiv = null;
				targetOffsetTop = null;
				targetOffsetLeft = null;
				wrapperOffsetTop = null;
				wrapperOffsetLeft = null;
				wrapperOffsetBottom = null;
				targetEl = null;
			}
		};
	}
}